#!/bin/bash

# Returns through stdout the calculated version

TAG="${CI_COMMIT_REF_NAME}"

# For master and develop use latest and SNAPSHOT respectively
if [[ ${TAG} == "master" ]]; then
    TAG="latest"
elif [[ ${TAG} == "develop" ]]; then
    TAG="SNAPSHOT"
else
    # Remove the gitflow branch type and keep the name
    TAG=$(echo ${CI_COMMIT_REF_NAME} | cut -f 2 -d "/")
fi

# Return version
echo "${TAG}"
