# Autoversioning

## Description
This tool provides an easy way to get the version in the current build. Now, it will return the following versions depending the branch:
* master -> latest
* develop -> snapshot
* another gitflow standard branch -> The *name* from *type*/*name*

## Usage
You can use it in your for example to build a docker image with the calculated tag like the example below:
```yaml
image: docker:stable
services:
  - docker:dind

variables:
  DOCKER_HOST: tcp://docker:2375
  DOCKER_DRIVER: overlay2
  IMAGE_NAME: $CI_REGISTRY_IMAGE

stages:
  - init
  - build

versioing:
  stage: init
  image: registry.gitlab.com/cattlelizers.io/autoversioning
  script:
    - /autoversioning.sh > version.txt
  artifacts:
    paths:
      - version.txt

build:
  stage: build
  script:
    - export TAG_VERSION=$(cat version.txt)
    - export IMAGE_TAG="${IMAGE_NAME}:${TAG_VERSION}"
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build -t $IMAGE_TAG .
    - docker push $IMAGE_TAG

``` 

## Letter to Santa
It would nice to have something like:
* master -> new *semver* to promote
* develop -> predicted *semver*-SNAPSHOT
